import DS from 'ember-data';
import { computed } from "@ember/object";

export default DS.RESTAdapter.extend({
    host: 'https://wt-762b2d00cf6da32a3e9d916b9f13a667-0.run.webtask.io/api',
    authorizer: 'authorizer:application',
    headers: computed(function() {
        'use strict';
        let token = JSON.parse(localStorage.getItem('ember_simple_auth:session'));
        if (token !== null) {
            return { "authorization": 'Bearer ' + token.authenticated.id_token };
        }
    })
});