import Component from '@ember/component';
import { inject as service } from "@ember/service";

export default Component.extend({
    store: service(),
    actions: {
        vote: function(event) {
            'use strict';
            let store = this.get('store');
            store.findRecord('event', event.id).then((event) => {
                event.incrementProperty('votes');
                event.save();
            });
        }
    }
});