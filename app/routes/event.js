import Route from '@ember/routing/route';

export default Route.extend({
    model(params) {
        'use strict';
        return this.store.findRecord('event', params.id);
    }
});